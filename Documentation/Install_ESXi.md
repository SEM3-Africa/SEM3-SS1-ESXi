## How to install ESXi

1. Download the images from VMware vSphere 6.5 Standard which contains the VMware vSphere Hypervisor (ESXi) 6.5.0d image.
2. Download [Rufus](https://rufus.akeo.ie/) which makes you able to create a bootable USB with the ESXi image on.
3. Get a USB flash drive and run Rufus and select the ESXi image to install on the flash drive. The USB flash drive is now bootable and able to install ESXi by plugging it into a server blade.
4. Plug the USB into your groups server blade.
5. Turn on the blade and when it starts up press F6 to get to the boot menu. Here you will choose the USB with the ESXi image.
6. Follow the installation instructions on the screen.
7. Unplug the USB and the ESXi software is now installed on the blades harddrive.
8. A standard ESXi screen will now show on the monitor. From here you can see the IP to connect to the webclient for setting up virtual machines etc from your PC. 