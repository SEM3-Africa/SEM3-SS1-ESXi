## What is ESXi and what are we using it for

Vmware ESXi is a hypervisor which allows us to install more machines into a machine.  
so we can etc. run more routers on 1 machine instead of wasting a whole machine  
on 1 router which takes nothing cpu and ram.

### Types of Hypervisors

There exist 2 types of hypervisor, a "Type 1" and "Type 2"  

### Type 1  

The type 1 hypervisor is a virtual machine manager which is installed on a bare metal pc.  
Which have nothing else to do other than manage the virtual machines which will be installed  
in the hypervisor.  

This method have better performance because the system dont have to take resources to run the  
base system on the computer. But can have limited support on the hardware, but many solutions can be  
done with a custom images with custom drivers to install esxi with

Known Type 1 hypervisor is: VMware ESXi - Microsoft HyperV - Citrix XenServer

### Type 2 

The Type 2 hypervisor is a software running on top of another OS on a computer so its not installed  
on a bare metal computer.

This method is a bit slower because the hypervisor have to ask the base system for hardware related stuff before  
it actually happens on the virtual machines. Also this method is more supported with hardware related stuff.

Known Type 2 hypervisors is: VMWare Workstation - VirtualBox - MS Virtual PC

## Access the WebClient

We have successfully installed ESXi 6.5 on our blade server in the server room for the "Project Network" assignment we have.  
And the ESXi 6.5 now has a web client where you can manage all the virtual machines instead of the old ESXi 5.5 and below  
where you need to install VSphere software to control the ESXi.

So if you want to access the web client open your favorite internet browser and go to the ip of the ESXi  
Then you will be prompted with a login screen where you have to enter the credentials for the server.

## Reference

All the information is gathered form the learning page lynda.com and from classes with Per.