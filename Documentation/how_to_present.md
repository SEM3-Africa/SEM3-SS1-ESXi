### Tips and tricks to a great presentation

* Keep a "red thread" through the presentation. A story from A-Z.
* Be sure to present an agenda in the beginning.
* Remember to present purpose, duration and audience.
* Make demos and show what is working. Break the powerpoint rhytm.
* Be sure to make every person equal. Equal talk time and equal question answering.
* Remember not to only recieve questions. Ask the audience some questions! 