This shows which VM is connected to which portgroup and virtual switch.

**vMXs**

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| vMX-Johan-vcp         | br-ext-johan       | Web-Client     |
|                       | br-int-johan       | br-int         |
| vMX-Johan-vfp         | br-ext-johan       | Web-Client     |
|                       | br-int-johan       | br-int         |
|                       | mgmt-vsrx          | p1p1           |

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| vMX-Lagos-vcp         | br-ext1-lagos     | br-ext1         |
|                       | br-int1-lagos     | br-int1         |
| vMX-Lagos-vfp         | br-ext1-lagos     | br-ext1         |
|                       | br-int1-lagos     | br-int1         |
|                       | mgmt-vsrx         | p1p1            |

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| vMX-Luand-vcp         | br-ext2-luand     | br-ext2         |
|                       | br-int2-luand     | br-int2         |
| vMX-Luand-vfp         | br-ext2-luand     | br-ext2         |
|                       | br-int2-luand     | br-int2         |
|                       | mgmt-vsrx         | p1p1            |


**Other**

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
| DNS4             | Africa          | Africa-MPLS            |
| DNS6             | Africa          | Africa-MPLS            |
| Docker           | Africa          | Africa-MPLS            |
| Radius-Auto      | Africa          | Africa-MPLS            |
| vSRX15           | vSRX_mgmt       | Web-Client             |
|                  | vSRX_mgmt       | Web-Client             |
|                  | mgmt-vsrx       | p1p1                   |
|                  | Africa          | Africa-MPLS            |
|                  | mgmt-vsrx       | p1p1                   |
|                  | mgmt-vsrx       | p1p1                   |

**Workstations**

|**VM**|**Portgroups**|**vSwitch**|
|--|--|--|
|ws-johan|ws-lan-johan|ws-lan-johan|
|ws-lagos|ws-lan-lagos|ws-lan-lagos|
|ws-luanda|ws-lan-luanda|ws-lan-luanda|

