## Description

This document contains the ordering of the ESXi reorder of the network adapters when using more than 4 adapters.

## Table

| Adapter |     | General vMX/vSRX/vQFX Interface 	| vMX Interfaces 	|
|:-------:| --- |:---------------------------------:|:-----------------:|
|  1	  | 	| fxp0	 						  	| EXT 		   		|
|  2	  | 	| internal link						| INT 				|	
|  3	  | 	| ge-0/0/0	 						| ge-0/0/0 			|
|  4	  | 	| ge-0/0/2	 						| ge-0/0/2			|
|  5	  | 	| ge-0/0/3	 						| ge-0/0/3			|
|  6	  | 	| ge-0/0/4	 						| ge-0/0/4			|
|  7	  | 	| ge-0/0/5	 						| ge-0/0/5			|
|  8	  | 	| ge-0/0/6	 						| ge-0/0/6			|
|  9	  | 	| ge-0/0/1	 						| ge-0/0/1			|

## Notes

The reordering happens because of the algorithm that VMware uses for mapping the vNICs.

## Sources

https://kb.vmware.com/s/article/2047927