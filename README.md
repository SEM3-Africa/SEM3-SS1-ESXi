# ESXI Weekly Plan

This document pertains to the weekly goals and plans in regards to Speical Subject 1 ESXI.

| Week | Working Period |Objectives | Additional Comments |
|---|---|---|---|
| 34 | 20/08/18 - 26/08/18 | Install ESXI, Study Lynda ESXI Material | 2 Hours Study Time |
| 35 | 27/08/18 - 02/09/18 | Write documentation | |
| 36 | 03/09/18 - 09/09/18 | Figure out ESXi networking, Install Test Env. stuff | |
| 37 | 10/09/18 - 16/09/18 | TBD | |
| 38 | 17/09/18 - 23/09/18 | TBD | |

This document is live and subject to change as of 27/08/2018.
